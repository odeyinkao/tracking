
import Chart from 'chart.js'

class PieChart {

    constructor(labels, bgColors, data, ref, updateFunction){

        this.updateFunction = updateFunction;
       
        this.data = {
            labels: labels,
            datasets: [
              {
                label: 'Dataset 1',
                data: data,
                backgroundColor: bgColors,
              }
            ]
          };

         this.config = {
            type: 'pie',
            data: this.data,
            options: {
              responsive: true,
              legend: {
                 position: 'bottom'
              }, 
              pointStyle: 'circle'
           }
          };

          this.chart = new Chart(ref, this.config);
    }


    addNewQuality = (changes) => {
      this.updateFunction(this.config.data.datasets[0].data, changes);
      this.chart.update();
    }

}

export default PieChart