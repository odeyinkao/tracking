import PieChart from './PieChart';

let Hooks = {};

Hooks.QualityPieChart = {
    mounted() {
        const qualities = JSON.parse(this.el.dataset.chartData)
        const labels = ['A-I', 'J-R', 'S-Z'];
        const bgColors = ['#E289F2', '#263238', '#855CF8' ];

        const prepData = (qualities) => {
            let data = [0, 0, 0];
            qualities.map(item => {
                if(item.name[0].toUpperCase() >= 'A' && item.name[0].toUpperCase() <= 'I' ){ data[0] += item.quality }
                if(item.name[0].toUpperCase() >= 'J' && item.name[0].toUpperCase() <= 'R' ){ data[1] += item.quality }
                if(item.name[0].toUpperCase() >= 'S' && item.name[0].toUpperCase() <= 'Z' ){ data[2] += item.quality }
            })

            return data
        }

        const updateData = (data, changes) => {
            if(changes.name[0].toUpperCase() >= 'A' && changes.name[0].toUpperCase() <= 'I' ){ data[0] += changes.quality }
            if(changes.name[0].toUpperCase() >= 'J' && changes.name[0].toUpperCase() <= 'R' ){ data[1] += changes.quality }
            if(changes.name[0].toUpperCase() >= 'S' && changes.name[0].toUpperCase() <= 'Z' ){ data[2] += changes.quality }

            return data;
        }

        this.chart = new PieChart(labels, bgColors, prepData(qualities), this.el, updateData)


        this.handleEvent("new-quality", ({name, quality}) => {
            debugger
            this.chart.addNewQuality({name, quality})
        })
    }
}

export default Hooks;