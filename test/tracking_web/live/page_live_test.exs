defmodule TrackingWeb.PageLiveTest do
  use TrackingWeb.ConnCase

  import Phoenix.LiveViewTest

  test "the quality-form should show field to create quality", %{conn: conn} do

    {:ok, page_live, disconnected_html} = live(conn, "/")

    assert disconnected_html =~ "Name"
    assert disconnected_html =~ "Quality"

    rendered = render(page_live)

    assert has_element?(page_live, "#name", nil)
    assert has_element?(page_live, "#quality", nil)

  end

  test "create new quality with quality-form", %{conn: conn} do

    {:ok, page_live, disconnected_html} = live(conn, "/")

    render(page_live)


    page_live
      |> form("#quality-form-for", quality: %{name: "Aldo Raine", quality: 8 })
      |> render_submit()


    assert has_element?(page_live, "#quality-table", "Aldo Raine")
    assert has_element?(page_live, "#quality-table", "8%")

    page_live
      |> form("#quality-form-for", quality: %{name: "Boba Feti", quality: 25 })
      |> render_submit()


    assert has_element?(page_live, "#quality-table", "Aldo Raine")
    assert has_element?(page_live, "#quality-table", "8%")
    assert has_element?(page_live, "#quality-table", "Boba Feti")
    assert has_element?(page_live, "#quality-table", "25%")
  end

  # test "handle_info/2", %{conn: conn} do
  #   {:ok, view, disconnected_html} = live(conn, "/")
  #   assert disconnected_html =~ "Count: 0"
  #   assert render(view) =~ "Count: 0"
  #   send(view.pid, %{payload: %{ val: 1 }})
  #   assert render(view) =~ "Count: 1"
  # end

end
