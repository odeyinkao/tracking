defmodule Tracking.Works.Quality do
  use Ecto.Schema
  import Ecto.Changeset

  schema "qualities" do
    field :name, :string
    field :quality, :integer

    timestamps()
  end

  @doc false
  def changeset(quality, attrs) do
    quality
    |> cast(attrs, [:name, :quality])
    |> validate_required([:name, :quality])
  end
end
