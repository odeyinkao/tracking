defmodule Tracking.Repo do
  use Ecto.Repo,
    otp_app: :tracking,
    adapter: Ecto.Adapters.Postgres
end
