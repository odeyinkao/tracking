defmodule TrackingWeb.PageLive do
  use TrackingWeb, :live_view

  alias Tracking.Works
  alias Tracking.Works.Quality

  def mount(_params, _session, socket) do
    if connected?(socket), do: Works.subscribe()

    changeset = Works.change_quality(%Quality{})

    {:ok, assign(socket,
                changeset: changeset,
                qualities: Works.list_qualities())}
  end

  def handle_info({:quality_created, quality}, socket) do
    socket = update(socket, :qualities, fn qualities -> [ quality | qualities] end )

    socket = push_event(socket, "new-quality", %{ :name => quality.name, :quality => quality.quality } )

    {:noreply, socket}
  end

  def handle_event("save", %{"quality" => params }, socket) do

    case Works.create_quality(params) do

      {:ok, _quality} ->
        changeset = Works.change_quality(%Quality{})
        {:noreply, assign(socket, changeset: changeset)}

      {:error, %Ecto.Changeset{} = changeset } ->
        socket = assign(socket, changeset: changeset)
        IO.inspect(changeset)
        {:noreply, socket}
    end

  end


end
