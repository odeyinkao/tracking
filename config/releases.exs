import Config
db_host = System.get_env("DATABASE_HOST") ||
  raise """
  environment variable DATABASE_HOST is missing.
  """
db_database = System.get_env("POSTGRES_DATABASE") || "tracking"
db_username = System.get_env("POSTGRES_USER") || "postgres"
db_password = System.get_env("POSTGRES_PASSWORD") || "postgres"
db_url = "ecto://#{db_username}:#{db_password}@#{db_host}/#{db_database}"
config :tracking, Tracking.Repo,
  url:  db_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")
secret_key = System.get_env("SECRET_KEY_BASE") || "vMpP6J5Gv0s9EoR8buZsvmkMKGwi0xSJnBcBNNRe+UfGGWLmMwIOIqUfr/rnW0WX"
secret_key_base = secret_key ||
  raise """
  environment variable SECRET_KEY_BASE is missing.
  You can generate one by calling: mix phx.gen.secret
  """
config :tracking, TrackingWeb.Endpoint,
  url: [host: "localhost", port: 4000],
  http: [:inet6, port: 4000],
  secret_key_base: secret_key_base
