defmodule Tracking.Repo.Migrations.CreateQualities do
  use Ecto.Migration

  def change do
    create table(:qualities) do
      add :name, :string
      add :quality, :integer

      timestamps()
    end

  end
end
