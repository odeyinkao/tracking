# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Tracking.Repo.insert!(%Tracking.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Tracking.Works

qualities = [
    %{name: "Aldo Raine",	quality: 8},
    %{name: "Boba Fett",	quality: 8},
    %{name: "Frank Furter",	quality: 9},
    %{name: "Mercellus Wallese", quality: 25},
    %{name: "Sofie Fatale",	quality: 100},
    %{name: "Zed",	quality: 50}
]

Enum.each(qualities, fn(quality) -> Works.create_quality(quality) end )
